using UnityEngine;
using UnityEngine.UI;



public class FormRegular : MonoBehaviour
{
    [SerializeField] private InputField _nameField;
    [SerializeField] private InputField _descriptionField;
    [SerializeField] private Dropdown _typeOfCharacterDropdown;
    [SerializeField] private Dropdown _classOfCharecterDropdown;

    [SerializeField] private Text _outputError;
    [SerializeField] private Image _formBackgroundPanel;
    private bool _isActive = false;


    public void UpdateInfoInForm()
    {
        if(_nameField.text.Length == 0)
        {
            _outputError.text = "Name is empty!";
            return;
        }

        string Name = _nameField.text;
        string Description = _descriptionField.text;
        string Race = _typeOfCharacterDropdown.options[_typeOfCharacterDropdown.value].text;
        string ClassOfCharacter = _classOfCharecterDropdown.options[_classOfCharecterDropdown.value].text;
    }

    private void Update()
    {
        isActiveForm();
        EnterToUpdateForm();
        
    }
    private void isActiveForm()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            _isActive = !_isActive;
            _formBackgroundPanel.gameObject.SetActive(_isActive);
        }
    }

    private void EnterToUpdateForm()
    {
        if(Input.GetKeyDown(KeyCode.KeypadEnter) && _isActive == true)
        {
            UpdateInfoInForm();
        }
    }
}
