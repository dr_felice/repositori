using UnityEngine;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IDropHandler
{
 
     public void OnDrop(PointerEventData eventData)
    {
        item.DragItem.transform.SetParent(transform);
    }
}
