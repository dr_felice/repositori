using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOnClick : MonoBehaviour
{
    [SerializeField] private AudioClip _audioClip;
    [SerializeField] private AudioSource _audioSrc;

    public void PlaySound()
    {
        if (!_audioSrc.isPlaying)
        {
            _audioSrc.clip = _audioClip;
            _audioSrc.Play();
        }
    }
    
}
