using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public GameConfig config;
    public ResourceType resourceType;
    public ResourceBank resourceBank;
    public Production production;

    public static Game instance;

    private void Awake()
    {
        instance = this;
        resourceBank = new ResourceBank(config);
        production = new Production(config);
    }
}


