using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Upgrader : MonoBehaviour
{
    public Button button;
    public Text buttonText;
    public ResourceType resourceType;

    Game _game;

    private void Awake()
    {
        _game = Game.instance;
        button.onClick.AddListener(OnClickHandler);
        UpdateText();
    }

    private void OnClickHandler()
    {
        _game.production.UpgradeProduction(resourceType);
        UpdateText();
    }

    private void UpdateText()
    {
        buttonText.text = $"Upgrade: {_game.production.GetLevel(resourceType)}lvl -> {_game.production.GetLevel(resourceType) + 1}lvl";
    }
}
