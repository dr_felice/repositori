using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ProductionBuilding : MonoBehaviour
{
    public ResourceType resourceType;
    public int count;
    public float productionTime;
    public Slider progressBar;
    Game _game;

    Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(OnClickHandler);
        progressBar.gameObject.SetActive(false);
        _game = Game.instance;
    }

    private void OnClickHandler()
    {
        Coroutine timer = StartCoroutine(ProductionRoutine());
    }

    IEnumerator ProductionRoutine()
    {
        button.interactable = false;
        float time = productionTime * (1 - (float)_game.production.GetLevel(resourceType) / 100);
        float timer = time;
        progressBar.gameObject.SetActive(true);
        while (timer > 0)
        {
            yield return null;
            timer -= Time.deltaTime;
            progressBar.value = 1 - (timer / time);
        }
        Game.instance.resourceBank.ChangeResource(resourceType, count);
        button.interactable = true;
        progressBar.gameObject.SetActive(false);
    }
}
