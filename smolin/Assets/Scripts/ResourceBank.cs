using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ResourceBank
{
    Dictionary<ResourceType, int> resources;
    public Action<ResourceType, int> onValueChanged;

    public ResourceBank(GameConfig config)
    {
        resources = new Dictionary<ResourceType, int>()
        {
            [ResourceType.FOOD] = config.foodCount,
            [ResourceType.HUMAN] = config.peopleCount,
            [ResourceType.WOOD] = config.woodCount,
            [ResourceType.STONE] = config.stoneCount,
            [ResourceType.GOLD] = config.goldCount
        };
    }

    public void ChangeResource(ResourceType type, int value) 
    {
        if (!resources.ContainsKey(type))
        {
            throw new InvalidDataException();
        }

        resources[type] += value;
        onValueChanged?.Invoke(type, value);
        
    }

    public int GetResource(ResourceType type)
    {
        if (!resources.ContainsKey(type))
        {
            throw new InvalidDataException();
        }
        return resources[type];
    }
}