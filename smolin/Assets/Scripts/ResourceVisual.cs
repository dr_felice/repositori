using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ResourceVisual : MonoBehaviour
{
    public ResourceType type;
    public Text textField;

    private Game _game;
    void Awake()
    {
        Game.instance.resourceBank.onValueChanged += ChangeValueHandler;
        _game = Game.instance;
        textField.text = _game.resourceBank.GetResource(type).ToString();
    }

    private void ChangeValueHandler(ResourceType type, int value)
    {
        if (type == this.type)
            textField.text = Game.instance.resourceBank.GetResource(type).ToString();
    }
}
